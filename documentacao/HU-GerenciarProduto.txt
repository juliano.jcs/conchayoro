Narrativa:

Como gerente
Quero cadastrar um produto
Para permitir realizar vendas

Critérios de aceitação:

Cenário 1
Dado que como gerente estou habilitado a cadastrar produtos
Quando registro um produto
Então o sistema cria um produto no estado “Criado”

Cenário 2
Dado que o produto está bloqueado
Quando atualizo o produto
Então o sistema atualiza o produto para o estado "bloqueado"
